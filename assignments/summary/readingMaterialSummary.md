# IOT Basics
## What is IOT?
 The Internet of Things (IoT) describes the network of physical objects—“things”—that are embedded with sensors, software, and other technologies for the purpose of connecting and exchanging data with other devices and systems over the internet.

 ![iot](https://40uu5c99f3a2ja7s7miveqgqu-wpengine.netdna-ssl.com/wp-content/uploads/2016/10/The-Internet-of-Things-from-connecting-devices-to-creating-value-large.jpg)


## IIOT
 The industrial internet of things (IIoT) refers to the extension and use of the internet of things (IoT) in industrial sectors and applications.The IIoT encompasses industrial applications, including robotics, medical devices, and software-defined production processes.

 ![iiot](https://altizon.com/wp-content/uploads/2018/02/altizon-systems-industrial-internet-of-things-banner.jpg)

## The Birth of IIOT..

## Industrial Revolution 
 ![img](https://pinaclsolutions.com/content/mixed-content/how-is-iot-helping-to-drive-the-4th-industrial-revolution/industrail-revolution.jpg)

## Industry 3.0
 The third industrial revolution uses electronics and IT systems to automate production, breaking the link between labour and manufactured goods.The third revolution brought forth the rise of electronics, telecommunications and of course computers. Through the new technologies, the third industrial revolution opened the doors to space expeditions, research, biotechnology and many more.

* ### Industry 3.0 Architecture
 Data is stored in databases and represented in excels.

 ![tt](https://gitlab.com/ganga1908/iotmodule1/-/raw/master/extras/pyramid.png)

* ### Industry 3.0 Communication Protocols
  * Modbus
  * PROFINET
  * CANopen
  * EtherCAT



## Industry 4.0
 Industry 4.0 is the information-intensive transformation of manufacturing (and related industries) in a connected environment of big data, people, processes, services, systems and IoT-enabled industrial assets with the generation, leverage and utilization of actionable data and information as a way and means to realize smart industry and ecosystems of industrial innovation and collaboration.

 ![iin](https://miro.medium.com/max/3312/1*DY0b8lMKQ8RPiFvTkZg4ew.png)

* ### Industry 4.0 Architecture
  Data data goes from controller to cloud via the industry 4.0 protocols.

  ![arch](https://i.pinimg.com/originals/e6/c6/9a/e6c69ad5912649a31831ff1a29ed8e9e.png)


* ### Major components of Industry 4.0
 At the very core Industry 4.0 includes the (partial) transfer of autonomy and autonomous decisions to cyber-physical systems and machines, leveraging information systems.

 ![ind4](https://ffrc.files.wordpress.com/2018/08/manu-11.png)

* ### Industry 4.0 Communication Protocols
     * MQTT
     * AMQP
     * CoAP
     * OPC UA
     * Websockets
     * HTTP
     * REST API

* ### Challenges
   * **Cost**:Industry 3.0 devices are very Expensive as a factory owner I dont want to switch to Industry 4.0 devices because they are expensive.
   * **Reliability**:Factory owners dont want to invest in devices which are unproven and unreliable.
   * **Information management** excellence as it’s all about actionable intelligence and connected information and process excellence in a context of relevance,    innovation and timely availability for any desired business, employee AND obviously customer goal.  
   * **Security and privacy**: The increasing number of attacks in the Industrial Internet of Things are a fact as IT and OT converge. Moreover, one of the main reasons which hold IIoT initiatives back are concerns regarding security and IIoT is, as said a key component of Industry 4.0.
   * The challenges regarding the integration of IT and OT.
   * Data compliance and management.
   * Managing risk and lowering costs in uncertain times.
   * A better understanding of IT and OT technologies and, more importantly, how they can be leveraged.
   * Altering customer and industrial partner demands.

* ### Solution
  Get data from Industry 3.0 devices/meters/sensors without changes to the original device and then send the data to the Cloud using Industry 4.0 devices.We have a library that helps get data from Industry 3.0 devices and send to Industry 4.0 cloud.
  
  ### How to get data from Industry 3.0?
   **Convert Industry 3.0 protocols to Industry 4.0 protocols**  

   ![img](https://gitlab.com/ganga1908/iotmodule1/-/raw/master/extras/conversion.png)

 * ### Challenges in Conversion
    * Expensive Hardware
    * Lack of documentaion
    * Properitary PLC protocols

* ### Industry 4.0 Vision

  ![ii](https://40uu5c99f3a2ja7s7miveqgqu-wpengine.netdna-ssl.com/wp-content/uploads/2017/01/Industry-4.0-strategy-a-staged-approach-with-value-opportunities-at-each-step.jpg)

* ## Summary
* ### Roadmap for making Industrial IoT product
   * Step 1 : Identify most popular Industry 3.0 devices.
   * Step 2 : Study Protocols that these devices communicate
   * Step 3 : Get data from the Industry 3.0 devices
   * Step 4 : Send the data to cloud for Industry 4.0
